import sys
sys.path.append('.')

from utilities import get_most_common_ngrams, DEFAULT_NGRAM_LOADING_RANGE, DEFAULT_N_IN_NGRAM_RANGE, SIGNALS

if __name__ == "__main__":
    for signal in SIGNALS:
        # common_ngrams is a dict of dicts of dicts
        # common_ngrams is first keyed by loading (float), then keyed by `signal`_`n`_grams (float), and
        # valued by {'ngrams': [...], 'exact_loading': float}
        get_most_common_ngrams(signal=signal,
                               ngram_loading_range=DEFAULT_NGRAM_LOADING_RANGE,
                               n_in_ngram_range=DEFAULT_N_IN_NGRAM_RANGE,
                               ngram_source='local')

