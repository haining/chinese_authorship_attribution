import os
import re
import pickle
import requests
import pandas as pd
from nltk import ngrams
import zhon.hanzi as zh
from zhon import pinyin
from typing import List
from codecs import encode
from itertools import chain
from bs4 import BeautifulSoup
from collections import Counter
from pypinyin import lazy_pinyin, Style
from random_user_agent.user_agent import UserAgent
from sklearn.model_selection import train_test_split


CORPORA = ['lx_zzr', 'cctaa', 'cross_register_vernacular', 'cross_register_classical']
SIGNALS = ['pinyin', 'utf8', 'stroke', 'character']
# get stroke_seq_lookup
STROKE_LOOKUP = {line.split("  ")[0]: line.split("  ")[1].replace(" ", "") for line in
                 open('corpus/word_stroke.txt').read().splitlines()}
DEFAULT_NGRAM_LOADING_RANGE = [.1, .2, .3, .4, .5, .6, .7, .8, .9]  # np.linspace(.1, .9, num=17)
DEFAULT_N_IN_NGRAM_RANGE = range(1, 7)  # from 1 to 6

# EXTENDED_N_IN_NGRAM_RANGE = list(range(1, 13))   # from 1 to 12


def get_data(corpus: str = 'lx_zzr'):
    train_raw, train_label, val_raw, val_label, test_raw, test_label = [], [], [], [], [], []

    if corpus.startswith("cross_register_"):
        register = corpus.split('_')[2]
        corpus_dir = os.path.join("corpus/cross-register-authorship-attribution-corpus-v1.0.0/corpus", register)
        for author_folder in os.scandir(corpus_dir):
            author_name = author_folder.name
            author_raw = [open(txt).read() for txt in os.scandir(author_folder.path) if txt.name.endswith("txt")]
            train_raw.extend(author_raw)
            train_label.extend([author_name] * len(author_raw))
        train_raw, test_raw, train_label, test_label = train_test_split(train_raw,
                                                                        train_label,
                                                                        test_size=0.1,
                                                                        random_state=42,
                                                                        shuffle=True)
    elif corpus == "cctaa":
        cctaa = pd.read_csv('corpus/cctaa-v1.0.0.csv')
        authors = set(cctaa.author.tolist())
        for split in ['train', 'validation', 'test']:
            for author in authors:
                author_raws = [re.sub('\s', '', t) for t in
                               cctaa.loc[(cctaa.author == author) & ((cctaa.split == split))].text.tolist()]
                author_names = [author] * len(author_raws)
                if split == 'train':
                    train_raw.extend(author_raws)
                    train_label.extend(author_names)
                elif split == 'validation':
                    val_raw.extend(author_raws)
                    val_label.extend(author_names)
                else:
                    test_raw.extend(author_raws)
                    test_label.extend(author_names)

    elif corpus == "lx_zzr":
        for text in os.scandir('corpus/lx_zzr_v1.0.0'):
            if text.name.startswith('train') and text.name.endswith('txt'):
                train_raw.append(open(text.path).read())
                train_label.append(text.name.split("_")[1])
            elif text.name.startswith('val') and text.name.endswith('txt'):
                # use val as test
                test_raw.append(open(text.path).read())
                test_label.append(text.name.split("_")[1])
            else:
                pass

    return {'train_raw': train_raw,
            'train_label': train_label,
            'val_raw': val_raw,
            'val_label': val_label,
            'test_raw': test_raw,
            'test_label': test_label}


def raw2signal(raw: str = '',
               signal: str = 'pinyin'):
    if signal == 'pinyin':
        return raw2pinyin(raw)
    elif signal == 'stroke':
        return raw2stroke(raw)
    elif signal == 'utf8':
        return raw2utf8(raw)
    elif signal == 'character':
        return raw2character(raw)


def raw2pinyin(raw):
    """
    Convert text to pinyin, carefully.

        TONE = 1¶
        标准声调风格，拼音声调在韵母第一个字母上（默认风格）。如： 中国 -> zhōng guó

        TONE2 = 2
        声调风格2，即拼音声调在各个韵母之后，用数字 [1-4] 进行表示。如： 中国 -> zho1ng guo2

        TONE3 = 8
        声调风格3，即拼音声调在各个拼音之后，用数字 [1-4] 进行表示。如： 中国 -> zhong1 guo2
    """
    # remove everything other than Chinese chars
    # remove things other than Chinese character and punctuation
    char_punck_string = ''.join([c for c in re.findall(
        '[{}]'.format(zh.characters + zh.punctuation), raw)])
    # get pinyin
    char_punck_pinyin_string = ''.join([c for c in lazy_pinyin(
        char_punck_string, style=Style.TONE,
        tone_sandhi=True,
        neutral_tone_with_five=False,
        v_to_u=False)])
    # remove punck
    pinyin_string = ''.join([c for c in re.findall(
        '[{}]'.format(pinyin.lowercase), char_punck_pinyin_string)])

    return pinyin_string


def get_stroke_from_zdict(a_character):
    user_agent_rotator = UserAgent(software_names=['chrome', 'chromium'],
                                   operating_systems=['windows', 'linux', 'mac', 'macos'], limit=100)

    try:
        resp = requests.post("http://www.zdic.net/hans/" + a_character,
                             headers={"User-Agent": user_agent_rotator.get_random_user_agent()})
        soup = BeautifulSoup(resp.content, "html.parser")
        stroke_string_ = soup.find("td", attrs={"class": "z_bis2"}).p.text
    except:
        stroke_string_ = ''
        print(f"Cannot find {a_character} on zdict.")
    return stroke_string_


def raw2stroke(raw,
               stroke_lookup=STROKE_LOOKUP,
               debug_mode=False):
    character_missing_stroke_seq = []
    # remove things other than Chinese character
    char_string = raw2character(raw)
    # get stroke seq
    stroke_string = ''
    for c in char_string:
        try:
            stroke_string += stroke_lookup[c]
        except:
            stroke_string_ = get_stroke_from_zdict(c)
            stroke_string += stroke_string_
            if stroke_string_ == '':
                character_missing_stroke_seq.append(c)
            else:
                STROKE_LOOKUP.update({c: stroke_string_})
    if debug_mode:
        return stroke_string, character_missing_stroke_seq
    else:
        return stroke_string


def raw2utf8(raw):
    # remove things other than Chinese character
    char_string = raw2character(raw)
    # get utf8 seq
    try:
        utf8_string = str(encode(char_string.encode(), "hex"), "utf-8")
    except:
        print(f"At least one character in {raw} cannot be converted to utf8 sequence.")

    return utf8_string


def raw2character(raw,
                  debug_mode=False):
    # remove things other than Chinese character
    char_string = ''.join([c for c in re.findall(
        '[{}]'.format(zh.characters), raw)])
    if not debug_mode:
        return char_string
    else:
        character_leaving_out = set(raw) - set(char_string)
        return char_string, character_leaving_out


def get_ngram(signal_string, n):
    ngrams_ = ngrams(signal_string, n=n)  # produces a generator containing tuple of strings, e.g., "('s', 'h', 'ē')"
    ngrams_ = [''.join(e) for e in list(ngrams_)]
    return ngrams_


def get_most_common_ngrams_from_signal_string(signal_string: str = '',
                                              n: int = 1,
                                              loading: float = 0.5):
    """
    It's hard to define "most common". Let's practically define most common ngrams as "ngrams compose at least a
    certain percent of all the ngrams." For example, most common 1-grams found in string "abcdefgggg" can be ['g']
    if we choose a threshold of 40%.
    """
    included_ngrams = []
    included_to_all_ratio = 0
    included_occurance = 0

    cnt = Counter(get_ngram(signal_string, n))
    total_ngrams = sum([v for k, v in cnt.most_common()])
    all_ngrams = cnt.most_common()
    for ngram_, occurance in all_ngrams:
        if included_to_all_ratio <= loading:
            included_ngrams.append(ngram_)
            included_occurance += occurance
            included_to_all_ratio = included_occurance / total_ngrams

    return {'ngrams': included_ngrams,
            'exact_loading': included_to_all_ratio}


def get_most_common_ngrams(signal: str = '',
                           ngram_loading_range: List[float] = DEFAULT_NGRAM_LOADING_RANGE,
                           n_in_ngram_range: List[int] = DEFAULT_N_IN_NGRAM_RANGE,
                           ngram_source: str = 'local'):
    """
    This module extracts the most common signal ngrams from a corpus, given percentage of a specific set of ngrams to
    all ngrams (or loading for short).
    """
    if ngram_source == 'local':
        for corpus in CORPORA:
            common_ngrams = {ld: {} for ld in ngram_loading_range}
            # common_ngrams is a dict of dicts of dicts
            # common_ngrams is first keyed by loading (float), then keyed by `signal`_`n`_grams (float), and
            # valued by {'ngrams': [...], 'exact_loading': float}

            # read in data with signal strings
            if not os.path.exists(f'{corpus}_signals.pkl'):
                raise AssertionError(f'Generate signals (with get_signal.py) from corpora first.')
            data = pickle.load(open(f'{corpus}_signals.pkl', 'rb'))
            # data = get_data(corpus=corpus)
            # data.update({f'train_{signal}': [raw2signal(raw, signal) for raw in data['train_raw']]})
            # data.update({f'val_{signal}': [raw2signal(raw, signal) for raw in data['val_raw']]})
            # data.update({f'test_{signal}': [raw2signal(raw, signal) for raw in data['test_raw']]})
            for loading in ngram_loading_range:
                # only use training data
                signal_string = ''.join(chain.from_iterable(data[f'train_{signal}']))
                for n in n_in_ngram_range:
                    common_ngrams[loading].update(
                        {f"{signal}_{n}_grams": get_most_common_ngrams_from_signal_string(
                            signal_string, n, loading)})
            pickle.dump(common_ngrams, open(f'corpus/{corpus}_frequent_{ngram_source}_{signal}_ngrams.pkl', 'wb'))
    elif ngram_source == 'global':
        raise NotImplemented("ngram_source == global not implemented.")
