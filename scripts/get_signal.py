import sys
sys.path.append('.')

import pickle
from utilities import get_data, raw2signal, CORPORA, SIGNALS


if __name__ == "__main__":
    for corpus in CORPORA:
        data = get_data(corpus=corpus)
        for signal in SIGNALS:
            data.update({f'train_{signal}': [raw2signal(raw, signal) for raw in data['train_raw']]})
            data.update({f'val_{signal}': [raw2signal(raw, signal) for raw in data['val_raw']]})
            data.update({f'test_{signal}': [raw2signal(raw, signal) for raw in data['test_raw']]})
        pickle.dump(data, open(f'corpus/{corpus}_signals.pkl', 'wb'))
