"""
1. finds high-frequency local/global signal ngrams given loading (percentage of a specific kind ngrams to all such
ngrams).
2. vectorizes data against the high-frequency signals
3. feeds vectorize data into machine learning pipeline
"""
__author__ = ("hw56@indiana.edu")
__version__ = "1.0.0"
__license__ = "ISC"

import os
import pickle
import sklearn
import argparse
import pandas as pd
from tqdm import tqdm
from sklearn.svm import SVC
from typing import List, Tuple
from collections import Counter
from sklearn.pipeline import Pipeline
from functionwords import FunctionWords
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import Normalizer, StandardScaler

import sys

sys.path.append('.')
from utilities import get_ngram, get_most_common_ngrams, get_data, raw2signal

CORPORA = ['cross_register_vernacular', 'cross_register_classical', 'lx_zzr', 'cctaa']
SIGNALS = ['pinyin', 'utf8', 'stroke', 'character']
DEFAULT_NGRAM_LOADING_RANGE = [.3, .4, .5, .6, .7, .8, ]  # np.linspace(.1, .9, num=17)
DEFAULT_N_IN_NGRAM_RANGE = range(1, 7)
# an example for CUSTOM_MODEL
CUSTOM_MODEL = []
# CUSTOM_MODEL: Tuple[str, sklearn.linear_model] = ('logistic regression', LogisticRegression(
#     C=1, solver="lbfgs", max_iter=1e9, multi_class="multinomial"))
CUSTOM_NGRAM_LOADING_RANGE = [.3, .4, .5, .6, .7]
CUSTOM_N_IN_NGRAM_RANGE = [1, 2, 3, 4, 5]


def main(signal: str = '',
         corpora: List[str] or str = CORPORA,
         ngram_loading_range: List[float] = DEFAULT_NGRAM_LOADING_RANGE,
         n_in_ngram_range: List[int] = DEFAULT_N_IN_NGRAM_RANGE,
         ngram_source: str = 'local',  # TODO
         model: str = 'standard_linear_svm'):
    # input sanity check
    assert signal in SIGNALS, AssertionError(f"`signal` should be in {SIGNALS}.")
    if corpora != 'all':
        assert len(set(corpora)) == len(corpora), AssertionError(
            f'Found duplicated corpora {[k for k, v in Counter(corpora).items() if v > 1]}.')
        assert set(CORPORA + ['all']) - set(corpora) == set(), AssertionError(
            f"`corpora` should be in {CORPORA + ['all']}.")
    else:
        corpora = CORPORA
    assert isinstance(ngram_loading_range, list), AssertionError(
        f"`ngram_loading_range` should be a list, not {type(ngram_loading_range)}.")
    for v in ngram_loading_range:
        assert isinstance(v, float), AssertionError(
            f"Values in `ngram_loading_range` should be a float, not {type(v)}.")
        assert 0.0 < v < 1.0, AssertionError(
            f"Values in `ngram_loading_range` should between 0 and 1 exclusive, not {str(v)}.")
    assert ngram_source in ['global', 'local']
    assert model in ['standard_linear_svm', 'custom']

    # get most common ngrams
    # first check if they have been extracted and saved on disk
    # if not, generate first

    # test accuracy and actual loading holder
    results = pd.DataFrame(
        columns=['corpus', 'signal', 'loading', 'up_to_ngram', 'ngram_len', 'nonzero_coef_len', 'model', 'test_acc'])
    for corpus in tqdm(corpora):
        print(f'Start dealing with {signal} on {corpus}')
        # read in data with signal strings
        if not os.path.exists(f'corpus/{corpus}_signals.pkl'):
            raise AssertionError(f'Generate signals (with get_signal.py) from corpora first.')
        data = pickle.load(open(f'corpus/{corpus}_signals.pkl', 'rb'))

        # load most common signal ngrams
        # check if most frequent signal has been extracted; if not, extract.
        if not os.path.exists(f'corpus/{corpus}_frequent_{ngram_source}_{signal}_ngrams.pkl'):
            raise AssertionError(f'Generate frequent signal ngrams (with get_frequent_ngrams.py) from corpora first.')
        common_ngrams = pickle.load(open(f'corpus/{corpus}_frequent_{ngram_source}_{signal}_ngrams.pkl', 'rb'))

        # specify a machine learning pipeline
        if model == 'standard_linear_svm':
            model_name = model
            model_instance = SVC(C=1, kernel="linear", max_iter=-1)
        elif model == 'custom':
            model_name, model_instance = CUSTOM_MODEL
        else:
            raise AssertionError(
                "Pass in `standard_linear_svm` or `custom` for model; if pass in `custom`, set CUSTOM_MODEL in the script.")
        pipeline = Pipeline([('normalizer', Normalizer(norm='l1')),
                             ('scaler', StandardScaler()),
                             (model_name, model_instance)])
        # labels won't change across baselines and loading settings
        y_train = data[f'train_label']
        y_test = data[f'test_label']

        # # running function character ngram baselines
        # print(f'Start calculating baselines on {corpus}')
        # for function_words_list in ['chinese_simplified_modern',
        #                             'chinese_classical_comprehensive',
        #                             'chinese_comprehensive']:
        #     fw = FunctionWords(function_words_list)
        #     X_train = [fw.transform(raw) for raw in data['train_raw']]
        #     X_test = [fw.transform(raw) for raw in data['test_raw']]
        #     # model fitting
        #     pipeline.fit(X_train, y_train)
        #     results.update({f'{corpus}_{signal}_{function_words_list}_test_accuracy': pipeline.score(X_test, y_test)})
        # feature engineering
        for loading in ngram_loading_range:
            print(f'Start doing loading {loading}')
            # X_train, X_test = [], []
            train_signal_string_list = data[f'train_{signal}']
            test_signal_string_list = data[f'test_{signal}']
            X_train_last, X_test_last = [], []
            for n in n_in_ngram_range:
                X_train, X_test = [], []

                for signal_string in train_signal_string_list:
                    X_train_ = []
                    cnt = Counter(get_ngram(signal_string, n))
                    for feat in common_ngrams[loading][f'{signal}_{n}_grams']['ngrams']:
                        X_train_.append(cnt.get(feat, 0))
                    X_train.append(X_train_)
                for signal_string in test_signal_string_list:
                    X_test_ = []
                    cnt = Counter(get_ngram(signal_string, n))
                    for feat in common_ngrams[loading][f'{signal}_{n}_grams']['ngrams']:
                        X_test_.append(cnt.get(feat, 0))
                    X_test.append(X_test_)
                # concatenate ngram with (n-1)gram
                if X_train_last != [] and X_train_last != []:
                    X_train = [[*lst1, *lst2] for lst1, lst2 in zip(X_train, X_train_last)]
                    X_test = [[*lst1, *lst2] for lst1, lst2 in zip(X_test, X_test_last)]

                # model fitting
                pipeline.fit(X_train, y_train)
                # columned by corpus, signal, loading, up_to_ngram, ngram_len, nonzero_coef_len, model, test_acc
                df_ = pd.DataFrame({'corpus': [corpus],
                                    'signal': [signal],
                                    'loading': [loading],
                                    'up_to_ngram': [n],
                                    'ngram_len': [len(X_train[0])],
                                    'nonzero_coef_len': [len(
                                        [v for v in pipeline.named_steps[model_name].coef_[0].tolist() if v != 0])],
                                    'model': [model_name],
                                    'test_acc': [pipeline.score(X_test, y_test)]})
                results = pd.concat([results, df_], ignore_index=True)
                print(f'up_to_ngram: {n} is done.')

    results.to_csv(f'results/{signal}_test_accuracy.csv', compression='gzip')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Reproduce results in the paper In Search of Better Representation for Chinese Authorsihp Attribution"
    )
    parser.add_argument(
        "-s",
        "--signal",
        default="pinyin",
        help="signal to use, allows 'pinyin', 'stroke', 'utf8', and 'character' ",
    )
    parser.add_argument(
        "-c",
        "--corpora",
        nargs='+',
        default="all",
        help=f"corpora for a specific signal, allows {CORPORA + ['all']}; set multiple as needed ",
    )
    parser.add_argument(
        "-l",
        "--ngram_loading_range",
        default="default",
        help="loading, ratio of ngrams to all ngrams, given n; if 'custom', remember set DEFAULT_NGRAM_LOADING_RANGE in train.py ",
    )
    parser.add_argument(
        "-n",
        "--n_in_ngram_range",
        default="default",
        help="n in ngram; if 'custom', remember set DEFAULT_N_IN_NGRAM_RANGE in train.py ",
    )
    parser.add_argument(
        "-i",
        "--ngram_source",
        default="local",
        help="ngrams to use, allows 'local' and 'global' ",
    )
    parser.add_argument(
        "-m",
        "--model",
        default="standard_linear_svm",
        help="model to use, allows 'standard_linear_svm' and 'custom'; if 'custom', remember set the model in train.py ",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__),
    )
    args = parser.parse_args()

    ngram_loading_range = DEFAULT_NGRAM_LOADING_RANGE if args.ngram_loading_range == 'default' else CUSTOM_NGRAM_LOADING_RANGE
    n_in_ngram_range = DEFAULT_N_IN_NGRAM_RANGE if args.n_in_ngram_range == 'default' else CUSTOM_N_IN_NGRAM_RANGE

    main(signal=args.signal,
         corpora=args.corpora,
         ngram_loading_range=ngram_loading_range,
         n_in_ngram_range=n_in_ngram_range,
         ngram_source=args.ngram_source,
         model=args.model)

    # # only for test
    # main(signal='stroke',
    #      corpora='all',
    #      ngram_loading_range=[.4, .5, .6],
    #      n_in_ngram_range=[1, 2, 3],
    #      ngram_source='local',
    #      model='standard_linear_svm')

# # debug
# corpus = 'cross_register_vernacular'
# ngram_source = 'local'
#
# data = pickle.load(open(f'{corpus}_signal.pkl', 'rb'))
# common_ngrams = pickle.load(open(f'corpus/{corpus}_frequent_{ngram_source}_ngrams.pkl', 'rb'))
