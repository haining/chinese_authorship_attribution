# Cross Register Authorship Attribution Corpus

Version 1.0

This corpus contains writings of eight authors known to have written in both vernacular and classical Chinese.

## Corpus Descriptions


| No. | Name_zh | Name_en/pinyin | Life span          | P.O.B                     | Title_zh         | Title_pinyin                             | Title_en                                                          | Alias      | Register   | Genre       | Rhymed  | Character_count (no punctuation) | Source                                                                                                                                                                                      |
|-----|---------|----------------|--------------------|---------------------------|------------------|------------------------------------------|-------------------------------------------------------------------|------------|------------|-------------|---------|----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | 冯梦龙   | Feng Menglong  | 1574-1646          | Suzhou, Jiangsu (江苏苏州)  | 燕都日記          | Yan Du Ri Ji                             | Yandu Diary                                                       | YDRJ       | classical  | non-fiction | no      | 13,752                           | https://ctext.org/wiki.pl?if=gb&chapter=765325                                                                                                                                              |
|     |         |                |                    |                           | 智囊全集           | Zhi Nang Quan Ji                         | Smart Ideas Pandect                                               | ZNQJ       | classical  | fiction     | no      | 43,376                           | https://www.tianyabook.com/xiazai/2014.html                                                                                                                                                 |
|     |         |                |                    |                           | 警世通言           | Xing Shi Tong Yan                        | Common Words to Alert the World                                   | JSTY       | vernacular | fiction     | no      | 329,382                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E8%AF%9D%E6%9C%AC/%E8%AD%A6%E4%B8%96%E9%80%9A%E8%A8%80.txt                                                       |
|     |         |                |                    |                           | 醒世恒言           | Xing Shi Heng Yan                        | Eternal Stories to Awaken the World                               | XSHY       | vernacular | fiction     | no      | 457,017                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E8%AF%9D%E6%9C%AC/%E9%86%92%E4%B8%96%E6%81%92%E8%A8%80.txt                                                       |
|     |         |                |                    |                           | 喻世明言           | Yu Shi Ming Yan                          | Illustrious Words to Instruct the World                           | YSMY       | vernacular | fiction     | no      | 317,583                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E8%AF%9D%E6%9C%AC/%E5%96%BB%E4%B8%96%E6%98%8E%E8%A8%80.txt                                                       |
|     |         |                |                    |                           | 醒名花             | Xing Ming Hua                            | Zhan GuoYing Romantic Story                                       | XMH        | vernacular | fiction     | no      | 65,839                           | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E9%86%92%E5%90%8D%E8%8A%B1.txt                                                                |
| 2   | 丁耀亢   | Ding Yaokang   | 1599-1669          | Zhucheng, Shandong (山东诸城) | 天史            | Tian Shi                                 | History of Order                                                  | TS         | classical  | non-fiction | no      | 61,908                           | https://zh.wikisource.org/zh/%E5%A4%A9%E5%8F%B2                                                                                                                                             |
|     |         |                |                    |                           | 续金瓶梅           | Xu Jin Ping Mei                          | Golden Plum Vase, A Continuation                                  | XJPM       | vernacular | fiction     | no      | 295,103                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E7%BB%AD%E9%87%91%E7%93%B6%E6%A2%85.txt                                                       |
| 3   | 祁彪佳   | Qi Biaojia     | 1603-1645          | Shaoxing, Zhejiang (浙江绍兴) | 远山堂剧品       |                                          | Opera Review from Yuanshan Studio                                 | YSTJP      | classical  | non-fiction | no      | 10,120                           | https://github.com/garychowcmu/daizhigev20/blob/master/%E8%AF%97%E8%97%8F/%E5%89%A7%E6%9B%B2/%E8%BF%9C%E5%B1%B1%E5%A0%82%E5%89%A7%E5%93%81.txt                                              |
|     |         |                |                    |                           | 远山堂曲品          |                                          | Qu Review from Yuanshan Studio                                    | YSTQP      | classical  | non-fiction | no      | 23,245                           | https://github.com/garychowcmu/daizhigev20/blob/master/%E8%AF%97%E8%97%8F/%E5%89%A7%E6%9B%B2/%E8%BF%9C%E5%B1%B1%E5%A0%82%E6%9B%B2%E5%93%81.txt                                              |
|     |         |                |                    |                           | 甲乙日历            |                                          | Diary of 1644 and 1645                                            | JYRL       | vernacular | non-fiction | no      | 54,755                           | https://github.com/garychowcmu/daizhigev20/blob/master/%E5%8F%B2%E8%97%8F/%E5%BF%97%E5%AD%98%E8%AE%B0%E5%BD%95/%E7%94%B2%E4%B9%99%E6%97%A5%E5%8E%86.txt                                     |
| 4   | 李渔     | Li Yu          | 1611-1680          | Jinhua, Zhejiang (浙江金华)| 闲情偶寄            | Xian Qing Ou Ji                          | Leisure Tales                                                     | XQOJ       | classical  | non-fiction | no      | 132,681                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E8%89%BA%E8%97%8F/%E7%BB%BC%E5%90%88/%E9%97%B2%E6%83%85%E5%81%B6%E5%AF%84.txt                                                       |
|     |         |                |                    |                           | 肉蒲团             | Rou Pu Tuan                              | The Carnal Prayer Mat                                             | RPT        | vernacular | fiction     | no      | 81,833                           | http://www.gutenberg.org/ebooks/52205                                                                                                                                                       |
|     |         |                |                    |                           | 十二楼             | Shi Er Lou                               | Twelve Mansions                                                   | SEL        | vernacular | fiction     | no      | 123,036                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E5%8D%81%E4%BA%8C%E6%A5%BC.txt                                                                |
|     |         |                |                    |                           | 无声戏             | Wu Sheng Xi                              | Silent Play                                                       | WSX        | vernacular | fiction     | no      | 110,827                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E6%97%A0%E5%A3%B0%E6%88%8F.txt                                                                |
| 5   | 褚人获   | Chu Renhuo     | 1635-？            | Suzhou, Jiangsu (江苏苏州)  | 坚瓠集             | Jian Hu Ji                               | Hard Gourd Collection                                             | JHJ        | classical  | non-fiction | no      | 604,762                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E5%AD%90%E8%97%8F/%E7%AC%94%E8%AE%B0/%E5%9D%9A%E7%93%A0%E9%9B%86.txt                                                                |
|     |         |                |                    |                           | 隋唐演义            | Sui Tang Yan Yi                          | Romance of Sui and Tang Dynasties                                 | STYY       | vernacular | fiction     | no      | 530,065                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E6%BC%94%E4%B9%89/%E9%9A%8B%E5%94%90%E6%BC%94%E4%B9%89.txt                                                       |
| 6   | 吴敬梓   | Wu Jingzi      | 1701-1754          | Chuzhou, Anhui (安徽滁州)   | 文木山房集          |                                          | Wenmu Collection                                                  | WMSFJ      | classical  | non-fiction | partial | 21,881                           | https://raw.githubusercontent.com/garychowcmu/daizhigev20/master/%E9%9B%86%E8%97%8F/%E5%9B%9B%E5%BA%93%E5%88%AB%E9%9B%86/%E6%96%87%E6%9C%A8%E5%B1%B1%E6%88%BF%E9%9B%86.txt                  |
|     |         |                |                    |                           | 儒林外史            |                                          | The Scholars                                                      | RLWS       | vernacular | fiction     | no      | 273,546                          | http://www.guoxuedashi.com/a/1095s/                                                                                                                                                         |
| 7   | 杜纲     | Du Gang        | ca. 1742- ca. 1800 | Kunshan, Jiangsu (江苏昆山)| 北史演义            |                                          | Romance of the Northern Dynasties                                 | BSYY       | classical  | fiction     | no      | 217,164                          | https://raw.githubusercontent.com/garychowcmu/daizhigev20/master/%E9%9B%86%E8%97%8F/%E6%BC%94%E4%B9%89/%E5%8D%97%E5%8C%97%E5%8F%B2%E6%BC%94%E4%B9%89.txt                                    |
|     |         |                |                    |                           | 南史演义            |                                          | Romance of the Southern Dynasties                                 | NSYY       | classical  | fiction     | no      | 160,125                          | https://raw.githubusercontent.com/garychowcmu/daizhigev20/master/%E9%9B%86%E8%97%8F/%E6%BC%94%E4%B9%89/%E5%8D%97%E5%8C%97%E5%8F%B2%E6%BC%94%E4%B9%89.txt                                    |
|     |         |                |                    |                           | 娱目醒心编           |                                          | Amusing and Awakening                                             | YMXXB      | vernacular | fiction     | no      | 109,196                          | https://raw.githubusercontent.com/garychowcmu/daizhigev20/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E5%A8%B1%E7%9B%AE%E9%86%92%E5%BF%83%E7%BC%96.txt                                    |
| 8   | 张曜孙   | Zhang Yaosun   | 1808-1863          | Yanghu, Jiangsu (江苏阳湖)  | 重订产孕集          | Chong Ding Chuan Yun Ji                  | Pregnancy and Childbirth, A Revision                              | CDCYJ      | classical  | non-fiction | no      | 25,407                           | http://www.360doc.com/document/19/0929/10/49670086_863845020.shtml                                                                                                                          |
|     |         |                |                    |                           | 续红楼梦未竟稿二十回  | Xu Hong Lou Meng Wei Jing Gao Er Shi Hui | Dream of the Red Chamber, An unfinished Twenty-Chapter Complement | XHLMWJGESH | vernacular | fiction     | no      | 141,440                          | https://github.com/garychowcmu/daizhigev20/blob/master/%E9%9B%86%E8%97%8F/%E5%B0%8F%E8%AF%B4/%E7%BB%AD%E7%BA%A2%E6%A5%BC%E6%A2%A6%E6%9C%AA%E7%AB%9F%E7%A8%BF%E4%BA%8C%E5%8D%81%E5%9B%9E.txt |


## Preprocessing

After downloading all texts, we performed the following preprocessing:

1. We checked all texts for flaws and missing parts by consulting other digital and print editions.
If multiple versions exist, we choose the one which has fewer characters missing or obvious errors. If a character in a 
text lacks a Unicode code point, we used the modern variant. 
We refer to [zdic.net](https://zdic.net) and [Unicode*pedia*](https://www.unicodepedia.com/) to check whether a character
falls outside of the Chinese Unicode set (between `\u4e00` and `\u9fff`). 
We keep outside characters if they are valid Chinese characters or punctuation, deleting them otherwise. 
Some valid Chinese characters fall outside of the aforementioned Unicode range. Characters are rarely deleted.

2. We remove title, heading, table of contents, preface, postscript, and editorial comments, as well as rhymed verse 
and prose if they are not part of the main body. Blank lines, redundant spaces, and indentation marks are removed too.

3. All the texts are automatically converted into UTF-8 encoded simplified Chinese using the Python 
package 'hanziconv' (v.0.3.2).

4. Texts are then segmented into roughly 1,000-character chunks without breaking clause-level structures. 
Modern publishers punctuate ancient Chinese texts, which originally did not have punctuation.
We leverage these delimiters introduced by editors to avoid breaking clauses when segmenting. 

5. We eliminate all punctuation to restore the original formatting.

6. After performing these steps, we organize all chunks by register under each candidate's directory with informative
file names.

Notice, we suggest splitting the works of authors who only have one work in a register to prevent inflating accuracy.
We refer you to the paper we published in the Second Conference on Computational Humanities Research (CHR 2021) for
justification (*The Challenge of Vernacular and Classical Chinese Cross-Register Authorship Attribution*).

## Public domain

All materials in this archive are in the public domain. All texts were published before 1900.

## Version History

- 1.0: Initial release.
